import time
import logging

from enum import Enum
from ch341 import CH341

# I2C address
PAJ7620_I2C_ADDRESS = 0x73
BANK_SELECT = 0xEF
GEST_INT = 0x43  # Gesture detection interruption
GEST_WAVE_INT = 0x44  # Gesture detection interruption

# Power up initialization
POWERUP_INIT = (
    (0xEF, 0x00),
    (0x37, 0x07),
    (0x38, 0x17),
    (0x39, 0x06),
    (0x41, 0x00),
    (0x42, 0x00),
    (0x46, 0x2D),
    (0x47, 0x0F),
    (0x48, 0x3C),
    (0x49, 0x00),
    (0x4A, 0x1E),
    (0x4C, 0x20),
    (0x51, 0x10),
    (0x5E, 0x10),
    (0x60, 0x27),
    (0x80, 0x42),
    (0x81, 0x44),
    (0x82, 0x04),
    (0x8B, 0x01),
    (0x90, 0x06),
    (0x95, 0x0A),
    (0x96, 0x0C),
    (0x97, 0x05),
    (0x9A, 0x14),
    (0x9C, 0x3F),
    (0xA5, 0x19),
    (0xCC, 0x19),
    (0xCD, 0x0B),
    (0xCE, 0x13),
    (0xCF, 0x64),
    (0xD0, 0x21),
    (0xEF, 0x01),
    (0x02, 0x0F),
    (0x03, 0x10),
    (0x04, 0x02),
    (0x25, 0x01),
    (0x27, 0x39),
    (0x28, 0x7F),
    (0x29, 0x08),
    (0x3E, 0xFF),
    (0x5E, 0x3D),
    (0x65, 0x96),
    (0x67, 0x97),
    (0x69, 0xCD),
    (0x6A, 0x01),
    (0x6D, 0x2C),
    (0x6E, 0x01),
    (0x72, 0x01),
    (0x73, 0x35),
    (0x74, 0x00),
    (0x77, 0x01),
)

# Gesture register initialization
GESTURE_INIT = (
    (0xEF, 0x00),
    (0x41, 0x00),
    (0x42, 0x00),
    (0xEF, 0x00),
    (0x48, 0x3C),
    (0x49, 0x00),
    (0x51, 0x10),
    (0x83, 0x20),
    (0x9F, 0xF9),
    (0xEF, 0x01),
    (0x01, 0x1E),
    (0x02, 0x0F),
    (0x03, 0x10),
    (0x04, 0x02),
    (0x41, 0x40),
    (0x43, 0x30),
    (0x65, 0x96),
    (0x66, 0x00),
    (0x67, 0x97),
    (0x68, 0x01),
    (0x69, 0xCD),
    (0x6A, 0x01),
    (0x6B, 0xB0),
    (0x6C, 0x04),
    (0x6D, 0x2C),
    (0x6E, 0x01),
    (0x74, 0x00),
    (0xEF, 0x00),
    (0x41, 0xFF),
    (0x42, 0x01),
)


# Gesture detection masks
class Gestures(Enum):
    GEST_UP = 0x01
    GEST_DOWN = 0x02
    GEST_LEFT = 0x04
    GEST_RIGHT = 0x08
    GEST_FORWARD = 0x10
    GEST_BACKWARD = 0x20
    GEST_CLOCKWISE = 0x40
    GEST_COUNTER_CLOCKWISE = 0x80
    GEST_WAVE = 0x100


class InitializationError(Exception):
    pass


class PAJ7620(object):
    def __init__(self, i2c_device, address=PAJ7620_I2C_ADDRESS):
        self._address = address
        self._device = i2c_device
        time.sleep(0.2)
        if self._read_u8(0x00) == 0x20:
            logging.debug(f"Gesture sensor init OK")
            # initializing powerup
            for i in range(len(POWERUP_INIT)):
                self._write_u8(POWERUP_INIT[i][0], POWERUP_INIT[i][1])
        else:
            logging.debug(f"Gesture sensor init error")
            raise InitializationError()
        self._write_u8(BANK_SELECT, 0)
        # initializing gesture detection
        for i in range(len(GESTURE_INIT)):
            self._write_u8(GESTURE_INIT[i][0], GESTURE_INIT[i][1])

    def _read_u8(self, register):
        return self._device.read_byte_data(self._address, register)

    def _read_u16(self, register):
        least_byte, most_byte = self._device.read_i2c_block_data(self._address, register, 2)
        return least_byte, most_byte

    def _write_u8(self, register, data):
        self._device.write_byte_data(self._address, register, data)

    def get_gesture(self):
        response = 0
        try:
            value = self._read_u16(GEST_INT)
            if value[0]:
                if value[1]:
                    response = (value[0] << 8) + value[1]
                else:
                    response = value[0]
        except Exception as e:
            # pass
            logging.error(f"error reading gesture: {e}")
        if response == Gestures.GEST_UP.value:
            response = Gestures.GEST_UP
        elif response == Gestures.GEST_DOWN.value:
            response = Gestures.GEST_DOWN
        elif response == Gestures.GEST_LEFT.value:
            response = Gestures.GEST_LEFT
        elif response == Gestures.GEST_RIGHT.value:
            response = Gestures.GEST_RIGHT
        elif response == Gestures.GEST_FORWARD.value:
            response = Gestures.GEST_FORWARD
        elif response == Gestures.GEST_BACKWARD.value:
            response = Gestures.GEST_BACKWARD
        elif response == Gestures.GEST_CLOCKWISE.value:
            response = Gestures.GEST_CLOCKWISE
        elif response == Gestures.GEST_COUNTER_CLOCKWISE.value:
            response = Gestures.GEST_COUNTER_CLOCKWISE
        return response
