import logging
import settings as sts

from logging.config import dictConfig
from pathlib import Path
from time import sleep
from ch341 import CH341
from paj7620 import PAJ7620, Gestures
from drv8830 import DRV8830

log_folder = Path(sts.LOG_FILE).parent
if not log_folder.exists():
    log_folder.mkdir(parents=True, exist_ok=True)

dictConfig(sts.LOGGING_CONFIG)

try:
    print("opening PAJ7620")

    i2c = CH341()
    i2c.set_speed(100)

    sensor = PAJ7620(i2c)
    pwm = DRV8830(i2c)

    while True:
        sleep(0.1)
        gesture = sensor.get_gesture()
        if gesture == Gestures.GEST_UP:
            logging.info("Up")
        elif gesture == Gestures.GEST_DOWN:
            logging.info("Down")
        elif gesture == Gestures.GEST_LEFT:
            logging.info("Left")
            pwm.brake()
            pwm.set_throttle(0)
        elif gesture == Gestures.GEST_RIGHT:
            logging.info("Right")
            pwm.reverse()
            pwm.set_throttle(.7)
        elif gesture == Gestures.GEST_FORWARD:
            logging.info("Forward")
            pwm.brake()
            pwm.set_throttle(0)
        elif gesture == Gestures.GEST_BACKWARD:
            logging.info("Backward")
            pwm.reverse()
            pwm.set_throttle(.7)
        elif gesture == Gestures.GEST_CLOCKWISE:
            logging.info("Clockwise")
        elif gesture == Gestures.GEST_COUNTER_CLOCKWISE:
            logging.info("Counter clockwise")

except Exception as e:
    logging.error(f"device error: {e}")
