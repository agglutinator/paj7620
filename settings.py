# ===== Logging settings =====
import logging
LOG_FILE = 'logs/paj7620.log'

# Logging
LOGGING_CONFIG = dict(
    version=1,
    formatters={
        # For files
        'detailed': {'format':
              '%(asctime)s %(levelname)-8s %(module)s: %(message)s'},
        # For the console
        'console': {'format':
              '%(asctime)s [%(levelname)-5s] %(module)s: %(message)s'}
    },
    handlers={
        'console': {
            'class': 'logging.StreamHandler',
            'level': logging.DEBUG,
            'formatter': 'console',
        },
        'file': {
            'class': 'logging.handlers.RotatingFileHandler',
            'level': logging.DEBUG,
            'formatter': 'detailed',
            'filename': LOG_FILE,
            'mode': 'a',
            'maxBytes': 1048576,  # 1 MB
            # 'maxBytes': 10485760,  # 10 MB
            'backupCount': 5
        }
    },
    root={
        'handlers': ['console', 'file'],
        'level': logging.DEBUG,
    },
    disable_existing_loggers=False
)
